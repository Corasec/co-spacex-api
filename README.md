## Co Spacex Api

To setup the project :

install packages:

`pip install -r requirements.txt`


In the folder co-spacex-api/spaceapi/spaceapi:

Create a `local_settings.py` file from the template in `local_settings_template.py`. You can also add any suitable constants

In the folder co-spacex-api/spaceapi:

`python manage.py migrate`

to run the project, in co-spacex-api/spaceapi:

`python manage.py runserver`

