from django.shortcuts import render
import requests
from django.http import HttpResponseNotFound
from django.core.exceptions import ObjectDoesNotExist
from .models import Capsule


def capsulespage(request):
    response = requests.get("https://api.spacexdata.com/v4/capsules").json()
    capsules_to_create = []
    for data in response:
        capsule = Capsule(
            base_id=data["id"],
            reuse_count=data["reuse_count"],
            water_landings=data["water_landings"],
            land_landings=data["land_landings"],
            last_update=data["last_update"],
            launches=data["launches"],
            serial=data["serial"],
            status=data["status"],
            type=data["type"],
        )
        capsules_to_create.append(capsule)

    Capsule.objects.bulk_create(capsules_to_create, ignore_conflicts=True)
    caps = Capsule.objects.all()

    print("caps lenght: ", caps.count())
    return render(request, "capsulespage.html", {"caps": caps})


def capsule(request, capsule_id):
    try:
        cap = Capsule.objects.get(id=capsule_id)
        return render(request, "capsule.html", {"cap": cap})
    except ObjectDoesNotExist:
        return HttpResponseNotFound("La capsule demandée n'existe pas")
