from django.apps import AppConfig


class SpacexapiConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "spacexapi"
