from django.db import models


class Capsule(models.Model):
    reuse_count = models.CharField(max_length=255, null=True)
    water_landings = models.CharField(max_length=255, null=True)
    land_landings = models.CharField(max_length=255, null=True)
    last_update = models.CharField(max_length=255, null=True)
    launches = models.JSONField(null=True)
    serial = models.CharField(max_length=255, null=True)
    status = models.CharField(max_length=255, null=True)
    type = models.CharField(max_length=255, null=True)
    base_id = models.CharField(max_length=255, null=True)

    def __str__(self):
        return str(self.base_id)
