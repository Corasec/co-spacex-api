import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

ALLOWED_HOSTS = ["*"]
INTERNAL_IPS = []

SECRET_KEY = (
    "django-insecure-c9a)#wldv*-0#2ocmrr%r_g581dl2z5g#=q1pz(@@8!lc0f5!#"  # noqa: 501
)

JWT_SECRET_KEY = "<Algo_Generate_a_JWT_Secret_Key>"

# set to False in production
DEBUG = True

INSTALLED_APPS = [
    # "debug_toolbar"
]

MIDDLEWARE = [
    # "debug_toolbar.middleware.DebugToolbarMiddleware",
]

INTERNAL_IPS = [
    "127.0.0.1",
]

STATIC_ROOT = "/home/user/co-spacex-api/spaceapi/static"

# Useful for uploaded files (such as company logos)
MEDIA_ROOT = ""
